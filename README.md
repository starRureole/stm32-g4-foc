# 基于STM32官方电调板B-G431B-ESC1开发的全自主FOC驱动程序

![B-G431B-ESC1](pics/B-G431-ESC1.jpg)

![update](pics/update.png)

### 介绍
该软件为在STM32官方开发板G431B-ESC1的无传感器FOC程序，支持参数自动识别、2~6s电压输入。该程序完全手写，不依赖任何不开源的电机库，开放此程序的原因为如果您手里有相关的电调硬件，可以烧入该程序进行性能测试，对比下ST库与本程序的性能差异。该程序可以适用于任何MCU，国产MCU在性能足够的前提下（**性能建议>150DMIPS**），且移植后的程序性能比ST官方的闭源电机库更好(TI的没测试过希望会做得更好2333)。在现在芯片大缺货的情况下，通过自研的FOC程序来摆脱对国外芯片的依赖越来越必要。详情请站内私信了解。

### 怎么购买B-G431B-ESC1
**从ST官网购买：** [B-G431B-ESC1](https://www.st.com/zh/evaluation-tools/b-g431b-esc1.html)  
**其他渠道，请自行去X宝、X鱼寻找。** 

### 使用说明

首先，为了烧录程序，请按照[教程](https://blog.csdn.net/Brendon_Tan/article/details/107686371)安装ST Cube Programer，**[点击此处下载](https://www.st.com/zh/development-tools/stm32cubeprog.html)**。

#### 下载完成后，将电调连接到电脑，打开cube programer，选择STLINK，然后连接(connect)
![打开](pics/ST_CUBE.png)
#### 打开hex文件
![hex](pics/open.png)
#### 下载到单片机
![hex](pics/download.png)

#### 焊接输入线缆
![pwm_in](pics/PWM_IN.png)
请焊接**5V、PWM、GND**引脚。这三个引脚请连接到PPM、PWM接收机。**该程序不再支持板载电位器**
### 连接到电调

首先，请关闭CubeProgramer，并给单片机重新上电。请安装**连接工具**下的**putty-64bit-0.74-installer.msi**   

![putty1](pics/putty1.png)

然后，打开putty，观察你是哪个串口，设置成串口模式。   

![putty1](pics/putty2.png)

最后，点击open,进入命令行。  

![putty1](pics/putty3.png)

### 测试您的电机参数并运行

***在这个步骤，请使用合适的电机，使用错误的电机可能触发电流保护***   

单击**tab**按键，可以看到所有命令。  

![putty1](pics/putty4.png)

```bash
cmd_set_current       CMD   --------  设置电机电流
cmd_stop_motor        CMD   --------  停止电机
cmd_start_motor       CMD   --------  启动电机
cmd_set_motor_rpm     CMD   --------  软件设置转速
cmd_test_motor_parameter    CMD   --------  测试电机参数
```

#### 连接电机到电调，输入下面命令进行测试（支持**tab**自动补全）
```bash
cmd_test_motor_parameter 10.0
```
*命令格式：测试命令+空格+参数，参数范围1.0~50.0，该参数根据目测电机功率大小来定，不知道直接给10.0。参数必须包含.0。否则程序不能正常识别。*  

#### 测试成功后，输出以下内容，都是正常数值表示测试成功，否则重新测试。
![putty1](pics/putty5.png)


#### 最后，输入开启命令，使电机旋转。请使用舵机测试仪、航模接收机进行测试。
![pwm](pics/pwm.png)
```bash
cmd_start_motor
```
![putty1](pics/putty6.png)

### 电机成功旋转！来一个电机全家福：

![motor](pics/motor.jpg)